# Lyrical-GraphQL

## How to run
Start the app ```npm run dev```
* http://localhost:4000
* http://localhost:4000/graphql

## GraphiQL Query Examples
```
mutation AddSong($title: String) {
  addSong(title: $title) {
    id
    title
  }
}

mutation DeleteSong($id: ID) {
  deleteSong(id: $id) {
    id
  }
}

mutation AddLyricToSong($songId: ID, $content: String) {
  addLyricToSong(songId: $songId, content: $content) {
    id
    lyrics {
      content
    }
  }
}

mutation LikeLyric($id: ID) {
  likeLyric(id: $id) {
    id
    likes
  }
}

query getSongs{
  songs {
    id
    title
    lyrics {
      id
      content
      likes
    }
  }
}


query getSongDetails($id: ID!){
  song(id: $id) {
    id
    title
    lyrics {
      content
    }
  }
}
```
