import React, { Component } from "react";
import fetchSongDetails from '../queries/fetchSongDetails'
import { graphql } from "react-apollo";
import { Link } from 'react-router'
import LyricCreate from "./LyricCreate";
import LyricList from "./LyricList";

class SongDetail extends Component {
    render() {
        const { song } = this.props.data;

        if (!song) {
            return <div>Loading...</div>
        }

        return (
            <div>
                <Link to="/" className="btn-floating btn blue ">
                    <i className="material-icons">keyboard_arrow_left</i>
                </Link>
                <h3>{song.title}</h3>
                <LyricList lyrics={song.lyrics} />
                <LyricCreate songId={this.props.params.id} />
            </div>
        );
    }
}


export default graphql(fetchSongDetails, {
    options: (props) => {
        return {
            variables: {
                id: props.params.id
            }
        }
    }
})(SongDetail);
