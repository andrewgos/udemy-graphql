import './style/style.css'

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import ApolloClient from 'apollo-client';
import { ApolloProvider } from 'react-apollo';

import App from './components/App'
import SongCreate from './components/SongCreate'
import SongDetail from './components/SongDetail'
import SongList from './components/SongList';

const client = new ApolloClient({
    /*
        note: this is to avoid doing refetchQueries method everytime (the refetchQueries has extra follow up request on every mutation)
        impact: for Apollo to know which item is which, so to auto-update accordingly whenever there is a new one
     */
    dataIdFromObject: o => o.id
});

const Root = () => {
  return  (
      <ApolloProvider client={client}>
          <Router history={hashHistory}>
              <Route path='/' component={App}>
                  <IndexRoute component={SongList} />
                  <Route path='songs/new' component={SongCreate} />
                  <Route path='songs/:id' component={SongDetail} />
              </Route>
          </Router>
      </ApolloProvider>
  );
};

ReactDOM.render(
  <Root />,
  document.querySelector('#root')
);
