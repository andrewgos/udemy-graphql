import gql from 'graphql-tag'; // available as dependency of apollo-client package

export default gql `
    query getSongDetails($id: ID!){
        song(id: $id) {
            id
            title
            lyrics {
                id
                content
                likes
            }
        }
    }
`;
