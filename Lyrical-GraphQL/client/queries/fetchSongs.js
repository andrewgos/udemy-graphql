import gql from 'graphql-tag'; // available as dependency of apollo-client package

export default gql `
    {
        songs {
            id
            title
        }
    }
`;
