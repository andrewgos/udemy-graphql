# auth-graphql-starter
GraphQL course on Udemy.com - Section 3!

## How to run
Start the app ```npm run dev```
* http://localhost:4000
* http://localhost:4000/graphql

## GraphiQL Query Examples
```
mutation SignUp($email: String, $password: String) {
  signup(email: $email, password: $password) {
    id
    email
  }
}

mutation Logout {
  logout {
    id
    email
  }
}

mutation Login($email: String, $password: String) {
  login(email: $email, password: $password) {
    id
    email
  }
}

query currentUser {
  user {
    id
    email
  }
}
```
