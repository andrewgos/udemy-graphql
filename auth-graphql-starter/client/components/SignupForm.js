import React, { Component } from "react";
import AuthForm from "./AuthForm";
import mutation from "../mutations/Signup"
import query from "../queries/CurrentUser"
import { graphql } from "react-apollo"
import { hashHistory } from "react-router";

class SignupForm extends Component {
    constructor(props) {
        super(props);

        this.state = { errors: [] }
    }

    componentWillUpdate(nextProps) {
        /*
            lifecycle notes:
            - `this.props` current set of props
            - `nextProps` the next set of props that will be in place when component re-renders
            why we do this:
            - using `this.props.mutate().then()` will have a race condition
                where the `then` is executed together with the refetchQueries
                this is a gotcha with the graphql mechanism
         */
        if (!this.props.data.user && nextProps.data.user) {
            hashHistory.push('/dashboard')
        }
    }

    onSubmit({ email, password }) {
        this.props.mutate({
            variables: { email, password },
            refetchQueries: [{ query }]
        }).catch(res => {
            const errors = res.graphQLErrors.map(error => error.message);
            this.setState({ errors })
        })
    }

    render() {
        return (
            <div className="container">
                <h3>Signup</h3>
                <AuthForm
                    errors={this.state.errors}
                    onSubmit={this.onSubmit.bind(this)}
                />
            </div>
        )
    }
}

// notes: order of injecting mutation vs query can be in any order
export default graphql(mutation)(
    graphql(query)(SignupForm)
);
