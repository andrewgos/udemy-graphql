import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient, { createNetworkInterface } from "apollo-client";
import { ApolloProvider } from "react-apollo";
import { Router, hashHistory, Route, IndexRoute } from "react-router";

import App from './components/App';
import LoginForm from "./components/LoginForm";
import SignupForm from "./components/SignupForm";
import Dashboard from "./components/Dashboard";
import requireAuth from "./components/requireAuth";

const networkInterface = createNetworkInterface({
    uri: '/graphql',
    opts: {
        credentials: 'same-origin' // same origin with the browser currently on (to send the cookies)
    }
});

// ApolloClient is the one who makes the request to the backend
// we are customizing the network interface, so that it will always include the cookie with the requests
const client = new ApolloClient({
    networkInterface,
    dataIdFromObject: o => o.id
})

const Root = () => {
  return (
      <ApolloProvider client={client}>
          <Router history={hashHistory}>
              <Route path="/" component={App}>
                  <Route path="login" component={LoginForm}></Route>
                  <Route path="signup" component={SignupForm}></Route>
                  <Route path="dashboard" component={requireAuth(Dashboard)}></Route>
              </Route>
          </Router>
      </ApolloProvider>
  );
};

ReactDOM.render(<Root />, document.querySelector('#root'));
