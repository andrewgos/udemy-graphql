## How to run
Start json-server ```npm run json:server```
* http://localhost:3000/users
* http://localhost:3000/companies

Start graphql express ```npm run start```
* http://localhost:4000/graphql

## GraphiQL Query Examples 
```
query findCompany {
  apple: company(id: "1") {
    ...companyDetails
    users {
      id
      firstName
      age
    }
  }
  google: company(id: "2") {
    ...companyDetails
    users {
      id
      firstName
      age
    }
  }
}

fragment companyDetails on Company {
  id
  name
  description
}

mutation add {
  addUser(firstName: "Stephen", age: 26) {
    id
    firstName
    age
  }
}

mutation delete {
  deleteUser(id: "EK5PGTV") {
    id
    firstName
    age
  }
}

mutation edit {
  editUser(id: "41", companyId: "2") {
    id
    firstName
    age
    company {
      id
    }
  }
}
```
